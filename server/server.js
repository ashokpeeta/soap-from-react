const express = require('express')
var fs = require('fs');
var xml2js = require('xml2js');
var parser = require('xml2js');
var xmlFile = './sample.xml';
const app = express()
const port = 7001

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', (req, res) => {
  fs.readFile(xmlFile, 'utf8', function(err, data){

    if (!err) {
      parser.parseString(data, function (err, result) {
               res.send(result);
           });
    }
    else {
      console.log(err);
    }
  })
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
